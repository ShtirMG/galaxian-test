﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Configs
{
    [CreateAssetMenu(menuName = "Galaxian/Galaxian Game Config", fileName = "GalaxianConfig")]
    public class GalaxianConfig : ScriptableObject
    {
        public ShipsConfig PlayerShips { get { return _playerShips; } }
        public EnemiesConfig Enemies { get { return _enemies; } }
        public LevelsConfig Levels { get { return _levels; } }
        public AttackRange[] AttackRanges { get { return _attackRanges; } }

        [SerializeField] private ShipsConfig _playerShips;
        [SerializeField] private EnemiesConfig _enemies;
        [SerializeField] LevelsConfig _levels;
        [SerializeField] AttackRange[] _attackRanges;

        [System.Serializable]
        public struct AttackRange
        {
            public float RestEnemiesPercent;
            public AttackTypes AttackType;
        }
    }

    public enum AttackTypes
    {
        EASY = 0,
        MEDIUM = 1,
        HARD = 2
    }
}