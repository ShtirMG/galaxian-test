﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Configs
{
    [CreateAssetMenu(menuName ="Galaxian/Levels Config", fileName = "LevelsConfig")]
    public sealed class LevelsConfig : ConfigsList<LevelConfig>
    {
    }
}