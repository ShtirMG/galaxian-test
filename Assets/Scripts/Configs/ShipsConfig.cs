﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Configs
{
    [CreateAssetMenu(menuName = "Galaxian/Ships Config", fileName = "ShipsConfig")]
    public sealed class ShipsConfig : ConfigsList<ShipConfig>
    {
    }
}