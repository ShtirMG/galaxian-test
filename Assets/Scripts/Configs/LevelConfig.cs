﻿using Galaxian.Gameplay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Configs
{
    [CreateAssetMenu(menuName = "Galaxian/New Level Config", fileName = "LevelConfig")]
    public class LevelConfig : ScriptableObject
    {
        public Sprite[] BackgroundSprites { get { return _backgroundSprites; } }
        public EnemyGrid[] EnemyGrids { get { return _enemyGrids; } }
        public float MaxTimeBetweenAttacks { get { return _maxTimeBetweenAttacks; } }
        public int Difficulty { get { return _difficulty; } }
        public int RepeatsMinimum { get { return _repeatsMinimum; } }

        [SerializeField] private Sprite[] _backgroundSprites;
        [SerializeField] private EnemyGrid[] _enemyGrids;
        [SerializeField] private float _maxTimeBetweenAttacks;
        [SerializeField] private int _difficulty;
        [SerializeField] private int _repeatsMinimum;
    }
}