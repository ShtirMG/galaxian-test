﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Configs
{
    public class ConfigsList<T> : ScriptableObject where T : ScriptableObject
    {
        public T[] Elements { get { return _elements; } }

        [SerializeField] private T[] _elements;
    }
}