﻿using Galaxian.Gameplay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Configs
{
    [CreateAssetMenu(menuName = "Galaxian/New Ship Config", fileName = "ShipConfig")]
    public class ShipConfig : ScriptableObject
    {
        public Ship Ship { get { return _ship; } }
        public Gun Gun { get { return _gun; } }
        public float MaxHealth { get { return _maxHealth; } }

        [SerializeField] private Ship _ship;
        [SerializeField] private Gun _gun;
        [SerializeField] private float _maxHealth;
    }
}