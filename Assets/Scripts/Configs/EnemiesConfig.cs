﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Configs
{
    [CreateAssetMenu(menuName = "Galaxian/Enemies Config", fileName = "EnemiesConfig")]
    public sealed class EnemiesConfig : ConfigsList<EnemyShipConfig>
    {
    }
}