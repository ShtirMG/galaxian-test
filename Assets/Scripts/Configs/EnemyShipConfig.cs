﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Configs
{
    [CreateAssetMenu(menuName ="Galaxian/New Enemy Config", fileName = "EnemyConfig")]
    public class EnemyShipConfig : ShipConfig
    {
        public float Speed { get { return _speed; } }
        public int Tier { get { return _tier; } }
        public int MinLevelToAppear { get { return _minLevelToAppear; } }
        public int MaxLevelToAppear { get { return _maxLevelToAppear; } }
        public int ScoreForKill { get { return _scoreForKill; } }

        [SerializeField] private float _speed;
        [SerializeField] private int _tier; //defines position in enemy grid (higher values means higher rows to initial stay)
        [SerializeField] private int _minLevelToAppear;
        [SerializeField] private int _maxLevelToAppear = -1; //minus one means no max restriction
        [SerializeField] private int _scoreForKill;
    }
}