﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Galaxian.Menu
{
    public class MainMenuView : View
    {
        public TextMeshProUGUI HighscoresText { get { return _highscoresText; } }

        [SerializeField] private Button _startButton;
        [SerializeField] private TextMeshProUGUI _highscoresText;

        protected override void Awake()
        {
            base.Awake();
            _startButton.onClick.AddListener(StartGame);
        }

        private void StartGame()
        {
            SceneManager.LoadScene(GameUtils.GAMEPLAY_LEVEL_NAME);
        }
    }
}