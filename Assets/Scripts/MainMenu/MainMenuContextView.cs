﻿using strange.extensions.context.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Menu
{
    public class MainMenuContextView : ContextView
    {
        private void Awake()
        {
            context = new MainMenuContext(this);
            context.Start();
        }
    }
}