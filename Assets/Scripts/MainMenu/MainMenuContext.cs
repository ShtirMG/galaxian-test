﻿using Galaxian.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Menu
{
    public class MainMenuContext : SignalContext
    {
        public MainMenuContext(MonoBehaviour view) : base(view)
        {
        }

        protected override void mapBindings()
        {
            base.mapBindings();
            mediationBinder.Bind<MainMenuView>().ToMediator<MainMenuMediator>();
        }
    }
}