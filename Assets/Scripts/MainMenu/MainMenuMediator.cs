﻿using Galaxian.Core.Services;
using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Menu
{
    public class MainMenuMediator : Mediator
    {
        [Inject]
        public MainMenuView View { get; set; }

        [Inject]
        public ISaveManager SaveManager { get; set; }

        public override void OnRegister()
        {
            base.OnRegister();
            if (SaveManager.SaveData.HighScores != 0)
            {
                View.HighscoresText.SetText(string.Format("HIGSCORE<br> {0}",SaveManager.SaveData.HighScores));
            }
            else
            {
                View.HighscoresText.SetText("");
            }
        }
    }
}