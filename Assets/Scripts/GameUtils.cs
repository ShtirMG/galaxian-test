﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian
{
    public static class GameUtils
    {
        public const string PLAYER_LAYER_NAME = "Player";
        public const string ENEMY_LAYER_NAME = "Enemies";

        public const string GET_DAMAGE_METHOD = "GetDamage";

        public const int MAX_ENEMY_GRID_COLUMNS = 10; //just check we fit into the level's corners and can do some moves

        public const string MENU_LEVEL_NAME = "MainMenu";
        public const string GAMEPLAY_LEVEL_NAME = "GameplayScene";
        public const string RUNNER_RESOURCE = "Runner";
    }
}