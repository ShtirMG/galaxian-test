﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Gameplay
{
    public class EnemyGrid : MonoBehaviour
    {
        public Vector2[][] EnemyCells { get; private set; }
        public float CellRadius { get { return _radius; } }

        [SerializeField] private GridRow[] _gridRows;
        [SerializeField] private float _radius = 0.5f;
        [SerializeField] private float _verticalPadding = 0.1f;

        public void CalculateGrid()
        {
            EnemyCells = null;

            if (_radius > 0f && _gridRows != null && _gridRows.Length > 0)
            {
                int rowsCount = _gridRows.Length;
                EnemyCells = new Vector2[rowsCount][];

                float initialVerticalPosition = (rowsCount - 1) * (_radius + _verticalPadding) * 0.5f;

                for (int i = 0; i < rowsCount; i++)
                {
                    if (_gridRows[i].Cols > 0)
                    {
                        if (_gridRows[i].Cols > GameUtils.MAX_ENEMY_GRID_COLUMNS)
                        {
                            Debug.LogWarningFormat("Enemy grid exceeds maximum length of grid in {0} row. Current length is {1}", i, _gridRows[i].Cols);
                        }

                        EnemyCells[i] = new Vector2[_gridRows[i].Cols];
                        float initialHorizontalPosition = -(_radius + _gridRows[i].Padding) * (_gridRows[i].Cols - 1) * 0.5f;

                        for (int j = 0; j < _gridRows[i].Cols; j++)
                        {
                            EnemyCells[i][j] = new Vector2(initialHorizontalPosition + (_radius + _gridRows[i].Padding) * j, initialVerticalPosition - (_radius + _verticalPadding) * i) * 2;
                        }
                    }
                }
            }
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            CalculateGrid();
        }

        private void OnDrawGizmos()
        {
            if (_radius > 0f && EnemyCells != null)
            {
                UnityEditor.Handles.color = Color.yellow;
                foreach (var row in EnemyCells)
                {
                    if (row != null)
                    {
                        foreach (var cell in row)
                        {
                            UnityEditor.Handles.DrawWireDisc(transform.position + (Vector3)cell, Vector3.forward, _radius);
                        }
                    }
                }
            }
        }
#endif
        [System.Serializable]
        private struct GridRow
        {
            public int Cols;
            public float Padding;
        }
    }
}