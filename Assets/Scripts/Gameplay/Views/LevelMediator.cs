﻿using DG.Tweening;
using Galaxian.Configs;
using Galaxian.Core.Services;
using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace Galaxian.Gameplay.Views
{
    public class LevelMediator : Mediator
    {
        private const float PLAYER_STARTING_TIME = 2f;
        private const int ALWAYS_ACCEPTABLE_TIER = 0; //There is can be several types of simpliest enemies on the level
        private const float DELAY_BETWEEN_ENEMY_APPEAR = 0.1f;
        private const float ENEMIES_SIDE_MOVE_DURATION = 4f;

        [Inject]
        public LevelView View { get; set; }

        [Inject]
        public ISaveManager SaveManager { get; set; }

        private int _currentLevelDifficulty = 0;
        private int _currentLevelRepeats = 0;
        private int _currentLevelNumber = 0;
        private int _originalEnemiesCount = 0;

        private int _currentScore = 0;

        private LevelConfig _currentLevelConfig;
        
        public override void OnRegister()
        {
            base.OnRegister();
            int playerShipIndex = SaveManager.SaveData.PlayerShip;
            playerShipIndex = Mathf.Clamp(playerShipIndex, 0, View.Config.PlayerShips.Elements.Length - 1);
            
            var playerConfig = View.Config.PlayerShips.Elements[playerShipIndex];
            var playerShip = Instantiate(playerConfig.Ship);
            var playerGun = Instantiate(playerConfig.Gun);
            View.Player.Init(playerShip, playerConfig.MaxHealth);
            View.Player.AddGun(playerGun);
            View.Player.PlayerDestroyedSignal.AddListener(OnPlayerDestroyed);

            View.EnemyController.AllEnemiesDestroyedSignal.AddListener(() => { EndGame(true); });
            View.EnemyController.ScoreRecievedSignal.AddListener(AddScores);
            View.EnemyController.AllEnemiesSendedToLevelSignal.AddListener(MovePlayerToLevel);

            StartGame();

            View.ContinueSignal.AddListener(StartGame);
            View.RestartSignal.AddListener(RestartGame);
            View.ReturnToMenuSignal.AddListener(ReturnToMenu);
        }

        private void ReturnToMenu()
        {
            SceneManager.LoadScene(GameUtils.MENU_LEVEL_NAME);
        }

        private void RestartGame()
        {
            _currentLevelConfig = null;
            _currentLevelRepeats = 0;
            _currentLevelNumber = 0;
            _currentScore = 0;
            View.Player.UI.SetScore(0);

            StartGame();
        }

        private void StartGame()
        {
            StopAllCoroutines();
            View.EnemyController.StopAllCoroutines();
            
            View.EndgamePanel.Toggle(false);
            View.Player.ResetPlayer();
            View.Player.transform.position = View.BottomAppearanceAnchor.position;
            CreateLevel();
        }
        
        private void MovePlayerToLevel()
        {
            StartCoroutine(MovePlayerToLevelRoutine());
        }

        private IEnumerator MovePlayerToLevelRoutine()
        {
            View.LevelMessageText.SetText("READY...");
            View.Player.CanBeMoved = false;
            View.Player.transform.position = View.BottomAppearanceAnchor.position;
            float currentTime = 0f;
            while (currentTime < PLAYER_STARTING_TIME)
            {
                currentTime += Time.deltaTime;
                View.Player.transform.position = Vector3.Lerp(View.BottomAppearanceAnchor.position, View.PlayerArrivingAnchor.position, currentTime / PLAYER_STARTING_TIME);
                yield return null;
            }

            View.Player.CanBeMoved = true;
            View.LevelMessageText.gameObject.SetActive(false);
        }

        private void AddScores(int scores)
        {
            _currentScore += scores;
            View.Player.UI.SetScore(_currentScore);
        }

        private void CreateLevel()
        {
            if (View.EnemyGrid != null)
            {
                Destroy(View.EnemyGrid.gameObject);
            }

            if (_currentLevelConfig == null) //if this is first run
            {
                _currentLevelConfig = View.Config.Levels.Elements.OrderBy(x => x.Difficulty).First();
            }
            else
            {
                if (_currentLevelRepeats < _currentLevelConfig.RepeatsMinimum)
                {
                    _currentLevelRepeats++;
                }
                else
                {
                    var newLevelConfig = View.Config.Levels.Elements.Where(x => x.Difficulty >= _currentLevelDifficulty && x.Difficulty <= _currentLevelDifficulty + 1).OrderBy(x => Random.value).First();
                    if (newLevelConfig != _currentLevelConfig)
                    {
                        _currentLevelRepeats = 0;
                    }
                    _currentLevelConfig = newLevelConfig;
                }
            }

            _currentLevelDifficulty = _currentLevelConfig.Difficulty;

            View.BackgroundImage.sprite = _currentLevelConfig.BackgroundSprites[Random.Range(0, _currentLevelConfig.BackgroundSprites.Length)];
            var currentEnemyGrid = Instantiate(_currentLevelConfig.EnemyGrids[Random.Range(0, _currentLevelConfig.EnemyGrids.Length)], View.EnemyGridAnchor, false);
            
            View.SetupEnemyGrid(currentEnemyGrid);

            var enemyConfigs = GetEnemyConfigsForCurrentLevel();
            var currentSpawnPoint = View.EnemySpawnPoints[Random.Range(0, View.EnemySpawnPoints.Length)];

            View.EnemyController.Init(View, enemyConfigs, _currentLevelConfig, currentSpawnPoint);

            _currentLevelNumber++;
            View.LevelMessageText.gameObject.SetActive(true);
            View.LevelMessageText.SetText(string.Format("LEVEL {0}", _currentLevelNumber));
        }
        
        private void EndGame(bool isWin)
        {
            if (!View.EndgamePanel.gameObject.activeSelf)
            {
                View.Player.Gun.StopShooting();
                View.Player.Gun.DestroyExistedBullets();
                View.Player.CanBeMoved = false;
                View.EndgamePanel.Init(_currentScore, isWin);
                StopAllCoroutines();
                View.EndgamePanel.Toggle(true);

                if (SaveManager.SaveData.HighScores < _currentScore)
                {
                    SaveManager.SaveData.HighScores = _currentScore;
                }

                SaveManager.Save();
            }
        }

        //returns list of enemy configs in tier order (cause we fill enemy grid from bottom (where weakest enemies is) to down)
        private List<EnemyShipConfig> GetEnemyConfigsForCurrentLevel()
        {
            int maxEnemies = View.EnemyGrid.EnemyCells.Length;

            List<EnemyShipConfig> resultConfigs = new List<EnemyShipConfig>();

            var enemyConfigs = View.Config.Enemies.Elements.Where(x => x.MinLevelToAppear <= _currentLevelNumber && (x.MaxLevelToAppear < 0 || x.MaxLevelToAppear >= _currentLevelNumber)).OrderBy(x => Random.value);

            foreach (var enemyConfig in enemyConfigs)
            {
                if (enemyConfig.Tier == ALWAYS_ACCEPTABLE_TIER || !resultConfigs.Any(x => x.Tier == enemyConfig.Tier))
                {
                    resultConfigs.Add(enemyConfig);
                }
            }

            Assert.IsTrue(resultConfigs.Count > 0, "Generator somehow failed to take enemies for level. Please check enemy configs");

            if (maxEnemies < resultConfigs.Count)
            {
                resultConfigs = resultConfigs.OrderBy(x => Random.value).Take(maxEnemies).ToList();
            }
            else if (maxEnemies > resultConfigs.Count) //fill rest of list with weakest enemies
            {
                resultConfigs = resultConfigs.OrderByDescending(x => x.Tier).ToList();
                while (maxEnemies > resultConfigs.Count)
                {
                    resultConfigs.Add(resultConfigs.Last());
                }
            }

            return resultConfigs.OrderByDescending(x => x.Tier).ToList();
        }
        
        private void OnPlayerDestroyed()
        {
            EndGame(false);
        }
    }
}