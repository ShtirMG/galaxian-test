﻿using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Galaxian.Gameplay.UI
{
    public class PlayerDragPanel : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        public readonly Signal<Vector2> BeginDragSignal = new Signal<Vector2>();
        public readonly Signal<Vector2> DragSignal = new Signal<Vector2>();
        public readonly Signal EndDragSignal = new Signal();

        public void OnBeginDrag(PointerEventData eventData)
        {
            BeginDragSignal.Dispatch(eventData.position);
        }

        public void OnDrag(PointerEventData eventData)
        {
            DragSignal.Dispatch(eventData.position);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            EndDragSignal.Dispatch();
        }
    }
}