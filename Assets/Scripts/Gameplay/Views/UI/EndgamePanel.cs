﻿using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Galaxian.Gameplay.UI
{
    public class EndgamePanel : MonoBehaviour
    {
        private const float SHOW_BUTTONS_DELAY = 2f;

        public readonly Signal ExitTapSignal = new Signal();
        public readonly Signal RestartTapSignal = new Signal();
        public readonly Signal ContinueTapSignal = new Signal();

        [SerializeField] private GameObject _buttonsContainer;
        [SerializeField] private Button _exitButton;
        [SerializeField] private Button _restartButton;
        [SerializeField] private Button _continueButton;

        [SerializeField] private TextMeshProUGUI _headerText;
        [SerializeField] private TextMeshProUGUI _scoreText;

        private void Awake()
        {
            _exitButton.onClick.AddListener(ExitTapSignal.Dispatch);
            _restartButton.onClick.AddListener(RestartTapSignal.Dispatch);
            _continueButton.onClick.AddListener(ContinueTapSignal.Dispatch);
        }

        public void Toggle(bool show)
        {
            gameObject.SetActive(show);
            if (show)
            {
                StartCoroutine(ShowButtonsRountine());
            }
        }

        private IEnumerator ShowButtonsRountine()
        {
            _buttonsContainer.SetActive(false);
            yield return new WaitForSecondsRealtime(SHOW_BUTTONS_DELAY); // delay to ensure all objects from previous level destroyed
            _buttonsContainer.SetActive(true);
        }

        public void Init(int scores, bool isWin)
        {
            _headerText.SetText(string.Format("YOU {0}!", isWin ? "WIN" : "LOSE"));
            _scoreText.SetText(string.Format("Your score: {0}", scores));
            _restartButton.gameObject.SetActive(!isWin);
            _continueButton.gameObject.SetActive(isWin);
        }

    }
}