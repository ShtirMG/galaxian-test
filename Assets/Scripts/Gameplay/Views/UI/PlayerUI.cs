﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Galaxian.Gameplay.UI
{
    public class PlayerUI : MonoBehaviour
    {
        public PlayerDragPanel DragPanel { get { return _playerDragPanel; } }

        [SerializeField] private Slider _healthBar;
        [SerializeField] private TextMeshProUGUI _scoreText;
        [SerializeField] private PlayerDragPanel _playerDragPanel;

        public void SetScore(int score)
        {
            _scoreText.SetText(score.ToString());
        }
        
        public void SetHealth(float healthPercent)
        {
            _healthBar.value = healthPercent;
        }
    }
}