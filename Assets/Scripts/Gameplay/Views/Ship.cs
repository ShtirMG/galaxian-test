﻿using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Gameplay
{
    [RequireComponent(typeof(Animator))]
    public class Ship : MonoBehaviour
    {
        public readonly Signal<GameObject> ShipExplosionSignal = new Signal<GameObject>(); //Vector2 - coordinates, GameObject - explosion object to spawn
        public readonly Signal<float> DamageRecievedSignal = new Signal<float>();

        private const string DESTROY_ANIMATION_TRIGGER = "Destroy";

        public Transform FireOrigin { get { return _fireOriginTransform; } }

        [SerializeField] private GameObject _shipSpriteObject;
        [SerializeField] private ParticleSystem[] _engineParticles;
        [SerializeField] private Transform _fireOriginTransform;
        [SerializeField] private ShipCollider _shipCollider;
        [SerializeField] private GameObject _explosionObject;
        [SerializeField] private bool _isDestroyAnimation;
        
        private Animator _animator;

        public void ResetShip()
        {
            if (_animator == null)
            {
                _animator = GetComponent<Animator>();
            }

            _shipSpriteObject.SetActive(true);
            _shipCollider.Collider.enabled = true;
            _shipCollider.DamageRecievedSignal.AddListener(DamageRecievedSignal.Dispatch);

            foreach(var engineParticle in _engineParticles)
            {
                engineParticle.Play();
            }
        }

        public void DestroyShip()
        {
            _shipCollider.Collider.enabled = false;
            _shipCollider.DamageRecievedSignal.RemoveListener(DamageRecievedSignal.Dispatch);
            if (_isDestroyAnimation)
            {
                _animator.SetTrigger(DESTROY_ANIMATION_TRIGGER);
            }
            else
            {
                OnShipDestroyed();
            }
        }

        public void SetShipLayer(int layer)
        {
            _shipCollider.gameObject.layer = layer;
        }

        //use as animation event if ship has destroy animation or simply call the method if don't
        private void OnShipDestroyed()
        {
            _shipSpriteObject.SetActive(false);
            if (_explosionObject != null)
            {
                ShipExplosionSignal.Dispatch(_explosionObject);
            }
        }
    }
}