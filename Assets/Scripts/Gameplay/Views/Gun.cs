﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Gameplay
{
    [RequireComponent(typeof(ParticleSystem))]
    public class Gun : MonoBehaviour
    {
        public float DelayForDestroy { get { return _gunParticleSystem.main.duration; } }

        [SerializeField] private float _cooldownTime = 0.05f;
        [SerializeField] private int _projectilesPerShoot = 1;
        [SerializeField] private float _damage = 1f;

        private ParticleSystem _gunParticleSystem;
        private float _currentCooldown = 0f;
        private int _pendingShoots = 0;

        private void Awake()
        {
            Init(true);
        }

        public void Init(bool isPlayerGun)
        {
            if (_gunParticleSystem == null)
            {
                _gunParticleSystem = GetComponent<ParticleSystem>();
            }

            var collision = _gunParticleSystem.collision;
            collision.collidesWith = (1 << LayerMask.NameToLayer((isPlayerGun) ? GameUtils.ENEMY_LAYER_NAME : GameUtils.PLAYER_LAYER_NAME));
        }

        public void Shoot()
        {
            _pendingShoots++;
        }

        public void StopShooting()
        {
            _pendingShoots = 0;
        }
        
        private void FixedUpdate()
        {
            if (_pendingShoots > 0 && _currentCooldown <= 0f)
            {
                MakeShot();
            }

            if (_currentCooldown > 0f)
            {
                _currentCooldown -= Time.deltaTime;
            }
        }

        private void MakeShot()
        {
            _pendingShoots--;
            _gunParticleSystem.Emit(_projectilesPerShoot);
            _currentCooldown = _cooldownTime;
        }

        public void DestroyExistedBullets()
        {
            _gunParticleSystem.Stop();
            _gunParticleSystem.Clear();
        }

        private void OnParticleCollision(GameObject other)
        {
            other.SendMessage(GameUtils.GET_DAMAGE_METHOD, _damage);
        }
    }
}