﻿using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Galaxian.Gameplay.Views
{
    public abstract class ShipView : View
    {
        public readonly Signal<ShipView> ShipChangeHealthSignal = new Signal<ShipView>();
        public readonly Signal<ShipView> ShipDestroyedSignal = new Signal<ShipView>();
        public readonly Signal<Vector2, GameObject> ShipExplosionSignal = new Signal<Vector2, GameObject>();

        public float HealthPercent { get { return _health / _maxHealth; } }
        public Gun Gun { get; private set; }
        
        protected float _health;
        protected float _maxHealth = 1f;

        protected Ship _ship;

        public virtual void Init(Ship ship, float maxHealth)
        {
            _ship = ship;
            _ship.ResetShip();
            _ship.transform.SetParent(transform);
            _ship.transform.localPosition = Vector2.zero;
            _ship.ShipExplosionSignal.AddListener(MakeExplosion);
            _ship.DamageRecievedSignal.AddListener(GetDamage);
            _maxHealth = maxHealth;
            _health = maxHealth;
        }

        public virtual void AddGun(Gun gun)
        {
            Gun = gun;
            Gun.transform.SetParent(_ship.FireOrigin);
            Gun.transform.localPosition = Vector3.zero;
            Gun.transform.localRotation = Quaternion.identity;
        }

        protected virtual void GetDamage(float damage)
        {
            _health -= damage;

            ShipChangeHealthSignal.Dispatch(this);

            if (_health <= 0f)
            {
                DestroyShip();
            }
        }

        private void MakeExplosion(GameObject explosion)
        {
            var explosionObject = Instantiate(explosion);
            explosionObject.transform.position = transform.position;
            Destroy(explosionObject.gameObject, 1f);
        }

        public virtual void DestroyShip()
        {
            ShipDestroyedSignal.Dispatch(this);
            _ship.DestroyShip();

            _ship.ShipExplosionSignal.RemoveListener(MakeExplosion);
            _ship.DamageRecievedSignal.RemoveListener(GetDamage);
        }
    }
}