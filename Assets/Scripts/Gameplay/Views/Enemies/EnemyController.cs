﻿using DG.Tweening;
using Galaxian.Configs;
using Galaxian.Gameplay.Views;
using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Galaxian.Gameplay
{
    public class EnemyController : MonoBehaviour
    {
        private const float ENEMIES_SIDE_MOVE_DURATION = 4f;

        public Signal AllEnemiesDestroyedSignal { get; } = new Signal();
        public Signal AllEnemiesSendedToLevelSignal { get; } = new Signal();
        public Signal<int> ScoreRecievedSignal { get; } = new Signal<int>();

        [SerializeField] private EnemyShipView _enemyShipViewPrefab;

        private LevelView _levelView;
        private LevelConfig _currentLevelConfig;

        private readonly List<EnemyShipView> _currentEnemies = new List<EnemyShipView>();

        private int _originalEnemiesCount = 0;

        public void Init(LevelView levelView, List<EnemyShipConfig> enemiesForLevel, LevelConfig levelConfig, Transform spawnPoint)
        {
            _levelView = levelView;
            _currentLevelConfig = levelConfig;
            
            foreach(var enemy in _currentEnemies)
            {
                if (enemy != null)
                {
                    Destroy(enemy.gameObject);
                }
            }

            _currentEnemies.Clear();

            CreateEneimes(enemiesForLevel, spawnPoint);
        }

        private void CreateEneimes(List<EnemyShipConfig> enemyConfigs, Transform spawnPoint)
        {
            for (int i = 0; i < _levelView.EnemyGrid.EnemyCells.Length; i++)
            {
                for (int j = 0; j < _levelView.EnemyGrid.EnemyCells[i].Length; j++)
                {
                    var enemy = Instantiate(_enemyShipViewPrefab, _levelView.EnemyGrid.transform);
                    enemy.gameObject.name = string.Format("Enemy_{0}_{1}", i, j);
                    enemy.transform.position = spawnPoint.position + (Vector3)_levelView.EnemyGrid.EnemyCells[i][j];
                    enemy.Init(enemyConfigs[i], _levelView.EnemyGrid.EnemyCells[i][j], new Vector2Int(i, j), _levelView);
                    enemy.ShipDestroyedSignal.AddOnce(OnEnemyDestroyed);
                    _currentEnemies.Add(enemy);
                }
            }
            _originalEnemiesCount = _currentEnemies.Count;

            StartCoroutine(MoveShipsToLevelRoutine());
        }


        private void OnEnemyDestroyed(ShipView ship)
        {
            var enemyShip = ship as EnemyShipView;

            if (enemyShip != null)
            {
                ScoreRecievedSignal.Dispatch(enemyShip.Config.ScoreForKill);

                if (_currentEnemies.Contains(enemyShip))
                {
                    _currentEnemies.Remove(ship as EnemyShipView);
                }
            }

            if (_currentEnemies.Count == 0)
            {
                AllEnemiesDestroyedSignal.Dispatch();
            }
        }


        private IEnumerator MoveShipsToLevelRoutine()
        {
            int enemiesReady = 0;

            foreach (var enemy in _currentEnemies)
            {
                enemy.SendToPoint(_levelView.EnemyGridAnchor.position + (Vector3)enemy.EnemyGridCell, false);
                enemy.EnemyReadyToAttackSignal.AddOnce(() => { enemiesReady++; });
                yield return new WaitForSeconds(0.25f);
            }

            AllEnemiesSendedToLevelSignal.Dispatch();

            while (enemiesReady < _currentEnemies.Count)
            {
                yield return null;
            }
            SetEnemyGridMoving();
            StartCoroutine(AttackRoutine());
        }

        private void SetEnemyGridMoving()
        {
            var maxGridHorizontalPosition = Mathf.Abs(_currentEnemies.Where(x => x.PositionInGrid.y == _currentEnemies.Max(y => y.PositionInGrid.y)).First().EnemyGridCell.x) + _levelView.EnemyGrid.CellRadius;

            var maxOffset = Mathf.Abs(_levelView.RightUpBound.position.x - maxGridHorizontalPosition);
            MoveGridRight(maxOffset);
        }

        private void MoveGridLeft(float value)
        {
            _levelView.EnemyGrid.transform.DOMove(_levelView.EnemyGridAnchor.position + new Vector3(value, 0, 0), ENEMIES_SIDE_MOVE_DURATION).OnComplete(() => MoveGridRight(-value));
        }

        private void MoveGridRight(float value)
        {
            _levelView.EnemyGrid.transform.DOMove(_levelView.EnemyGridAnchor.position + new Vector3(value, 0, 0), ENEMIES_SIDE_MOVE_DURATION).OnComplete(() => MoveGridLeft(-value));
        }


        private IEnumerator AttackRoutine()
        {
            float delay = 1f;
            while (_currentEnemies.Count > 0)
            {
                var attackRange = _levelView.Config.AttackRanges.OrderByDescending(x => x.RestEnemiesPercent).Where(x => x.RestEnemiesPercent <= (float)_currentEnemies.Count / _originalEnemiesCount).FirstOrDefault();
                bool isSimpleAttack = attackRange.AttackType == AttackTypes.EASY && Random.value > 0.5f;
                bool isLeftSide = Random.value > 0.5f;
                var enemies = GetShipsToAttack(attackRange.AttackType, isSimpleAttack, isLeftSide);
                if (enemies != null && enemies.Length > 0)
                {
                    foreach (var enemy in enemies)
                    {
                        if (isSimpleAttack)
                        {
                            enemy.MakeSimpleAttack();
                        }
                        else
                        {
                            if (enemy.IsReadyToAttack)
                            {
                                enemy.MakeAttackManeuver(isLeftSide, attackRange.AttackType == AttackTypes.HARD);
                            }
                        }
                    }
                }

                delay = Mathf.Clamp((_currentLevelConfig.MaxTimeBetweenAttacks * _currentEnemies.Count / _originalEnemiesCount), 1f, _currentLevelConfig.MaxTimeBetweenAttacks);

                yield return new WaitForSeconds(delay);
            }
        }

        private EnemyShipView[] GetShipsToAttack(AttackTypes attackType, bool isSimpleAttack, bool isLeftSide)
        {
            if (_currentEnemies.Count > 0)
            {
                switch (attackType)
                {
                    case AttackTypes.MEDIUM:
                        var enemies = GetEnemiesForManeuver(isLeftSide);
                        if (enemies.Length > 3)
                        {
                            enemies = enemies.Take(3).ToArray();
                        }

                        return enemies;
                    case AttackTypes.HARD:
                        return new EnemyShipView[] { _currentEnemies[Random.Range(0, _currentEnemies.Count)] };
                    default:
                        if (isSimpleAttack)
                        {
                            return _currentEnemies.Where(enemy => enemy.PositionInGrid.x >= _currentEnemies.Max(x => x.PositionInGrid.x) && enemy.IsReadyToAttack).OrderBy(x => Random.value).Take(1).ToArray();
                        }
                        else
                        {

                            enemies = GetEnemiesForManeuver(isLeftSide);
                            return new EnemyShipView[] { enemies[0] };
                        }
                }
            }

            return null;
        }


        private EnemyShipView[] GetEnemiesForManeuver(bool isLeftSide)
        {
            var enemyRows = _currentEnemies.Where(enemy => enemy.IsReadyToAttack && (enemy.PositionInGrid.x >= _currentEnemies.Max(x => x.PositionInGrid.x) - 1));
            return enemyRows.Where(enemy => (enemy.PositionInGrid.y == enemyRows.Max(x => x.PositionInGrid.y) && !isLeftSide) || (enemy.PositionInGrid.y == enemyRows.Min(x => x.PositionInGrid.y) && isLeftSide)).OrderBy(x => Random.value).ToArray();
        }
    }
}