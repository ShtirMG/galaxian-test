﻿using DG.Tweening;
using Galaxian.Configs;
using Galaxian.Core;
using Galaxian.Gameplay.Views;
using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Gameplay.Views
{
    public class EnemyShipView : ShipView
    {
        private const float DEFAULT_ANGLE = 0;
        private const int PATH_ATTACK_INDEX = 3; //we try to attack at the middle of the path
        
        public readonly Signal EnemyReadyToAttackSignal = new Signal();

        public Vector2 EnemyGridCell { get; private set; }

        public Vector2Int PositionInGrid { get; private set; }

        public EnemyShipConfig Config { get; private set; }

        public bool IsReadyToAttack { get; private set; }

        private LevelView _levelView;

        public void Init(EnemyShipConfig config, Vector2 enemyGridCell, Vector2Int positionInGrid, LevelView levelView)
        {
            Config = config;
            var ship = Instantiate(config.Ship);
            var gun = Instantiate(config.Gun);
            Init(ship, config.MaxHealth);
            EnemyGridCell = enemyGridCell;
            PositionInGrid = positionInGrid;
            _levelView = levelView;
            AddGun(gun);
        }

        public override void Init(Ship ship, float maxHealth)
        {
            base.Init(ship, maxHealth);
            _ship.SetShipLayer(LayerMask.NameToLayer(GameUtils.ENEMY_LAYER_NAME));
        }

        public override void AddGun(Gun gun)
        {
            base.AddGun(gun);
            Gun.Init(false);
        }

        public void SendToPoint(Vector2 point, bool isLocalMovement)
        {
            transform.up = point - (Vector2)((isLocalMovement) ? transform.localPosition : transform.position);
            var defaultAngle = new Vector3(0, 0, DEFAULT_ANGLE);
            var duration = Vector2.Distance(transform.position, point) / Config.Speed;
            transform.DORotate(defaultAngle, duration).SetEase(Ease.InBack);
            if (isLocalMovement)
            {
                transform.DOLocalMove(point, duration).OnComplete(() => { IsReadyToAttack = true; EnemyReadyToAttackSignal.Dispatch(); });
            }
            else
            {
                transform.DOMove(point, duration).OnComplete(() => { IsReadyToAttack = true; EnemyReadyToAttackSignal.Dispatch(); });
            }
        }

        public void MakeSimpleAttack()
        {
            Gun.transform.LookAt(transform.position-Vector3.up);
            Gun.Shoot();
            Gun.Shoot();
        }

        public void MakeAttackManeuver(bool isLeft, bool isContinious)
        {
            transform.SetParent(null);
            IsReadyToAttack = false;
            bool isAlreadyAttackOnSight = false;
            var path = CreateAttackPath(_levelView.EnemyGrid.transform.position.x, _levelView.BottomAppearanceAnchor.position.y, isLeft);
            var initialPosition = _levelView.EnemyGrid.transform.position + (Vector3)EnemyGridCell;
            transform.DOPath(path, GetPathDistance(transform.position, path) / Config.Speed * 2f, PathType.CatmullRom, PathMode.Sidescroller2D).
                SetEase(Ease.InSine).
                SetLookAt(0.01f, null, transform.right).
                OnUpdate(() => {
                    Gun.transform.LookAt(Gun.transform.position-Vector3.up);
                    if (!isAlreadyAttackOnSight && Mathf.Abs(_levelView.Player.transform.position.x - transform.position.x) < _levelView.EnemyGrid.CellRadius)
                    {
                        isAlreadyAttackOnSight = true;
                        Gun.Shoot();
                    }
                }).
                OnWaypointChange((int index) => { OnManeuverWaypointChange(index == PATH_ATTACK_INDEX); }).
                OnComplete(() => {
                    if (isContinious && HealthPercent > 0)
                    {
                        ContinueAttack();
                    }
                    else
                    {
                        ReturnToPositionAfterManeuver();
                    }
                });
        }

        private void ReturnToPositionAfterManeuver()
        {
            transform.SetParent(_levelView.EnemyGrid.transform);
            transform.position = new Vector3(_levelView.EnemyGrid.transform.position.x + EnemyGridCell.x, _levelView.TopAnchorForEnemyRestart.position.y);
            SendToPoint(EnemyGridCell, true);
        }

        private void OnManeuverWaypointChange(bool isAttackPoint)
        {
            if (isAttackPoint)
            {
                Gun.Shoot();
                Gun.Shoot();
            }
        }

        private Vector3[] CreateAttackPath(float currentGridHorizontalPosition, float bottomPosition, bool isLeft)
        {
            Vector3[] result = new Vector3[4];
            result[0] = transform.position + new Vector3(((isLeft) ? -2.5f : 2.5f) * _levelView.EnemyGrid.CellRadius, 0, 0); //first point is always right aside of ship
            result[1] = result[0] + new Vector3(((isLeft) ? -1f : 1f)*_levelView.EnemyGrid.CellRadius, -2*_levelView.EnemyGrid.CellRadius, 0); //we must avoid ships on bottom
            result[2] = new Vector2(currentGridHorizontalPosition, (result[1].y + bottomPosition) / 2f + 2*_levelView.EnemyGrid.CellRadius); //second point always at the horizontal middle of enemy grid
            result[3] = new Vector2(-transform.position.x, bottomPosition); //endPoint always on opposite to initial horizontal side

            return result;
        }

        private void ContinueAttack()
        {
            transform.position = new Vector3(_levelView.EnemyGrid.transform.position.x + EnemyGridCell.x, _levelView.TopAnchorForEnemyRestart.position.y);
            MakeAttackManeuver(Random.value > 0.5f, true);
        }

        private float GetPathDistance(Vector3 origin, Vector3[] path)
        {
            var distance = Vector3.Distance(origin, path[0]);
            if (path.Length > 1)
            {
                for (int i = 1; i < path.Length; i++)
                {
                    distance += Vector3.Distance(path[i - 1], path[i]);
                }
            }
            return distance;
        }

        public override void DestroyShip()
        {
            base.DestroyShip();
            Gun.StopShooting();
            transform.DOKill();
            Destroy(gameObject, Gun.DelayForDestroy);
        }
    }
}