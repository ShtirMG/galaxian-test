﻿using Galaxian.Configs;
using Galaxian.Gameplay.UI;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Galaxian.Gameplay.Views
{
    public class LevelView : View, ILevelView
    {
        public Signal ReturnToMenuSignal { get; } = new Signal();
        public Signal RestartSignal { get; } = new Signal();
        public Signal ContinueSignal { get; } = new Signal();

        public PlayerShipView Player { get { return _player; } }
        public GalaxianConfig Config { get { return _config; } }

        public Transform BottomAppearanceAnchor { get { return _bottomAppearanceAnchor; } }
        public Transform PlayerArrivingAnchor { get { return _playerArriveAnchor; } }
        public Transform EnemyGridAnchor { get { return _enemyGridAnchor; } }
        public Transform TopAnchorForEnemyRestart { get { return _topAnchorForEnemyRestart; } }
        public Transform LeftDownBound { get { return _leftDownBound; } }
        public Transform RightUpBound { get { return _rightUpBound; } }
        public Transform[] EnemySpawnPoints { get { return _enemySpawnPoints; } }

        public EnemyGrid EnemyGrid { get; private set; }

        public SpriteRenderer BackgroundImage { get { return _backgroundImage; } }
        public TextMeshProUGUI LevelMessageText { get { return _levelMessageText; } }
        public EnemyController EnemyController { get { return _enemyController; } }
        public EndgamePanel EndgamePanel { get { return _endgamePanel; } }

        [SerializeField] private PlayerShipView _player;
        [SerializeField] private GalaxianConfig _config;
        [SerializeField] private EnemyController _enemyController;

        [Header("Anchors and level bounds")]
        [SerializeField] private Transform _bottomAppearanceAnchor;
        [SerializeField] private Transform _playerArriveAnchor;
        [SerializeField] private Transform _enemyGridAnchor;
        [SerializeField] private Transform _leftDownBound;
        [SerializeField] private Transform _rightUpBound;
        [SerializeField] private Transform _topAnchorForEnemyRestart;
        [SerializeField] private Transform[] _enemySpawnPoints;

        [Header("Level decorations")]
        [SerializeField] private SpriteRenderer _backgroundImage;
        [SerializeField] private TextMeshProUGUI _levelMessageText;
        [SerializeField] private EndgamePanel _endgamePanel;

        protected override void Awake()
        {
            base.Awake();
            _endgamePanel.ExitTapSignal.AddListener(ReturnToMenuSignal.Dispatch);
            _endgamePanel.RestartTapSignal.AddListener(RestartSignal.Dispatch);
            _endgamePanel.ContinueTapSignal.AddListener(ContinueSignal.Dispatch);
        }

        public void SetupEnemyGrid(EnemyGrid enemyGrid)
        {
            EnemyGrid = enemyGrid;
            EnemyGrid.CalculateGrid();
        }
    }
}