﻿using Galaxian.Configs;
using Galaxian.Gameplay.UI;
using Galaxian.Gameplay.Views;
using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Galaxian.Gameplay
{
    public interface ILevelView
    {
        PlayerShipView Player { get; }

        Transform LeftDownBound { get; }
        Transform RightUpBound { get; }
    }
}