﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Gameplay.Views
{
    public class PlayerCameraView : View
    {
        public Camera Camera { get { return _camera; } }

        [SerializeField] private Camera _camera;
    }
}