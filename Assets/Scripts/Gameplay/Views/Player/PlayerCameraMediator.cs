﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Gameplay.Views
{
    public class PlayerCameraMediator : Mediator
    {
        private const float CAMERA_SPEED = 2f;

        [Inject]
        public ILevelView LevelView { get; set; }

        [Inject]
        public PlayerCameraView View { get; set; }

        private float _maxOffset = 0f;
        private Vector3 _cameraPosition;

        public override void OnRegister()
        {
            base.OnRegister();

            _cameraPosition = View.Camera.transform.position;
            var screenLeftCorner = View.Camera.ScreenToWorldPoint(Vector2.zero);
            _maxOffset = Mathf.Abs(LevelView.LeftDownBound.position.x - screenLeftCorner.x) + 0.25f;
            StartCoroutine(CameraMove());
        }

        private IEnumerator CameraMove()
        {
            float nextCameraPosition = 0f;
            float currentCameraPosition = _cameraPosition.x;
            while(true)
            {
                nextCameraPosition = Mathf.Clamp(LevelView.Player.transform.position.x, -_maxOffset, _maxOffset);
                if(Mathf.Abs(_cameraPosition.x - nextCameraPosition) > 0.1f)
                {
                    _cameraPosition.x = Mathf.Lerp(_cameraPosition.x, nextCameraPosition, Time.deltaTime*CAMERA_SPEED);
                    View.Camera.transform.position = _cameraPosition;
                }
                yield return null;
            }
        }

    }
}