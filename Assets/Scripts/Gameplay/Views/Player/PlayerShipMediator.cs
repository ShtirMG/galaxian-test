﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Gameplay.Views
{
    public class PlayerShipMediator : Mediator
    {
        private const float MOVE_SPEED = 25f;

        [Inject]
        public PlayerShipView View { get; set; }

        [Inject]
        public ILevelView LevelView { get; set; }

        private Vector2 _beginDragPoint;
        private Vector2 _beginShipPoint;
        private Vector2 _destinationPoint;
        private Vector2 _pointToMove;
        
        public override void OnRegister()
        {
            base.OnRegister();
            View.UI.DragPanel.BeginDragSignal.AddListener(OnBeginDrag);
            View.UI.DragPanel.DragSignal.AddListener(OnDrag);
            View.UI.DragPanel.EndDragSignal.AddListener(OnEndDrag);
        }

        private void OnBeginDrag(Vector2 point)
        {
            _beginDragPoint = Camera.main.ScreenToWorldPoint(point);
            _beginShipPoint = View.transform.position;
            View.IsMoving = true;
        }

        private void OnDrag(Vector2 point)
        {
            _destinationPoint = Camera.main.ScreenToWorldPoint(point);
            _pointToMove = _beginShipPoint + (_destinationPoint - _beginDragPoint);
            _pointToMove.x = Mathf.Clamp(_pointToMove.x, LevelView.LeftDownBound.position.x, LevelView.RightUpBound.position.x);
            _pointToMove.y = Mathf.Clamp(_pointToMove.y, LevelView.LeftDownBound.position.y, LevelView.RightUpBound.position.y);
        }

        private void OnEndDrag()
        {
            View.IsMoving = false;
            _destinationPoint = View.transform.position;
            View.Gun.StopShooting();
        }

        private void FixedUpdate()
        {
            if (View.IsMoving && View.CanBeMoved)
            {
                View.transform.position = Vector2.MoveTowards(View.transform.position, _pointToMove, MOVE_SPEED*Time.deltaTime);
                View.Gun.Shoot();
            }
        }
    }
}