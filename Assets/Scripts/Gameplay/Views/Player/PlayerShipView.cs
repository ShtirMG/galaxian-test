﻿using Galaxian.Gameplay.UI;
using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Gameplay.Views
{
    public class PlayerShipView : ShipView
    {
        public readonly Signal PlayerDestroyedSignal = new Signal();

        public bool CanBeMoved { get; set; }
        public bool IsMoving { get; set; }
        public PlayerUI UI { get { return _playerUI; } }
        
        [SerializeField] private PlayerUI _playerUI;
        
        public override void Init(Ship ship, float maxHealth)
        {
            base.Init(ship, maxHealth);
            _playerUI.SetHealth(1);
            _playerUI.SetScore(0);
            _ship.SetShipLayer(LayerMask.NameToLayer(GameUtils.PLAYER_LAYER_NAME));
        }

        public void ResetPlayer()
        {
            _playerUI.SetHealth(1);
            _health = _maxHealth;
            _ship.ResetShip();
        }

        protected override void GetDamage(float damage)
        {
            base.GetDamage(damage);
            _playerUI.SetHealth(HealthPercent);
        }

        public override void DestroyShip()
        {
            ShipDestroyedSignal.Dispatch(this);
            _ship.DestroyShip();
            CanBeMoved = false;
            PlayerDestroyedSignal.Dispatch();
        }

        public override void AddGun(Gun gun)
        {
            base.AddGun(gun);
            Gun.Init(true);
        }
    }
}