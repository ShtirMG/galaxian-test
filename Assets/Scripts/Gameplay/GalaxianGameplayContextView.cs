﻿using strange.extensions.context.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Gameplay
{
    public class GalaxianGameplayContextView : ContextView
    {
        private void Awake()
        {
            context = new GalaxianGameplayContext(this);
            context.Start();
        }
    }
}