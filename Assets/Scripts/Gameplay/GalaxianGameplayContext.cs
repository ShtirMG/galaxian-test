﻿using Galaxian.Core;
using Galaxian.Core.Services;
using Galaxian.Gameplay.Views;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Gameplay
{
    public class GalaxianGameplayContext : SignalContext
    {
        public GalaxianGameplayContext(MonoBehaviour view) : base(view)
        {
        }

        protected override void mapBindings()
        {
            base.mapBindings();

            var levelView = Object.FindObjectOfType<LevelView>();

            injectionBinder.Bind<ILevelView>().ToValue(levelView);

            mediationBinder.Bind<LevelView>().ToMediator<LevelMediator>();
            mediationBinder.Bind<PlayerShipView>().ToMediator<PlayerShipMediator>();
            mediationBinder.Bind<PlayerCameraView>().ToMediator<PlayerCameraMediator>();
        }
    }
}