﻿using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Gameplay
{
    [RequireComponent(typeof(Collider2D))]
    public class ShipCollider : MonoBehaviour
    {
        public Collider2D Collider { get; private set; }

        public readonly Signal<float> DamageRecievedSignal = new Signal<float>();

        private void Awake()
        {
            Collider = GetComponent<Collider2D>();
        }

        private void GetDamage(float damage)
        {
            DamageRecievedSignal.Dispatch(damage);
        }
    }
}