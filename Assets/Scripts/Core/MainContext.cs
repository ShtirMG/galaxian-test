﻿using Galaxian.Core.Services;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Core
{
    public class MainContext : SignalContext
    {
        public MainContext(MonoBehaviour view) : base(view)
        {
        }

        protected override void mapBindings()
        {
            base.mapBindings();

            injectionBinder.Bind<ISaveManager>().To<SaveManager>().ToSingleton().CrossContext();
            injectionBinder.Bind<ISaveProvider>().To<PlayerPrefsSaveProvider>().CrossContext();
        }
    }
}