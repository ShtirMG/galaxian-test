﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace Galaxian.Core.Services
{
    public sealed class PlayerPrefsSaveProvider : ISaveProvider
    {
        private const string SAVE_DATA_NAME = "GalaxianSaveData";

        public PlayerSave Load()
        {
            string stringData = PlayerPrefs.GetString(SAVE_DATA_NAME, null);

            if (!string.IsNullOrEmpty(stringData))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(PlayerSave));
                PlayerSave saveData;
                try
                {
                    using (TextReader reader = new StringReader(stringData))
                    {
                        saveData = serializer.Deserialize(reader) as PlayerSave;
                    }
                }
                catch
                {
                    Debug.LogError("Cant deserialize!");
                    saveData = new PlayerSave();
                }
                return saveData;
            }
            return new PlayerSave();
        }

        public void Save(PlayerSave save)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(PlayerSave));

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, save as PlayerSave);
                PlayerPrefs.SetString(SAVE_DATA_NAME, writer.ToString());
            }
            PlayerPrefs.Save();
        }
    }
}