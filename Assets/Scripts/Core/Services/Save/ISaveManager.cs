﻿namespace Galaxian.Core.Services
{
    public interface ISaveManager
    {
        PlayerSave SaveData { get; }
        
        void Save();
        void Load();
    }
}