﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Core.Services
{
    public interface ISaveProvider
    {
        void Save(PlayerSave save);
        PlayerSave Load();
    }
}