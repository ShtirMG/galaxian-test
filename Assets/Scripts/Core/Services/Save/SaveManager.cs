﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian.Core.Services
{
    public class SaveManager : ISaveManager
    {
        public PlayerSave SaveData
        {
            get
            {
                if (_saveData == null)
                {
                    Load();
                }
                return _saveData;
            }
        }

        [Inject]
        public ISaveProvider SaveProvider { get; set; }

        private PlayerSave _saveData;

        public void Save()
        {
            SaveProvider.Save(SaveData);
        }

        public void Load()
        {
            _saveData = SaveProvider.Load();
        }
    }
}