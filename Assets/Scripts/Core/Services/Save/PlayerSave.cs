﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace Galaxian.Core.Services
{
    [XmlRoot("PlayerSave")]

    public class PlayerSave
    {
        public int HighScores { get; set; }
        public int PlayerShip { get; set; }
    }
}