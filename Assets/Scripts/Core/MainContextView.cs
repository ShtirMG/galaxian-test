﻿using strange.extensions.context.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Galaxian.Core
{
    public class MainContextView : ContextView
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void StartUp()
        {
            var runner = Resources.Load<MainContextView>(GameUtils.RUNNER_RESOURCE);
            if (runner != null)
            {
                var go = Instantiate(runner);
                DontDestroyOnLoad(go);
            }
        }

        private void Awake()
        {
            context = new MainContext(this);
            context.Start();
            SceneManager.LoadScene(GameUtils.MENU_LEVEL_NAME);
        }
    }
}